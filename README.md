# DS2019_30642_Mitisor_Maria_Assignment_2
install erlang and rabbitmq   
open in terminal the installation directory of rabbitmq    
type 'rabbitmq-plugins enable rabbitmq_management'
log into rabbitmq (port 15672) with credentials guest/guest
run app as spring boot app    
access endpoint /rabbit-app/producer. It will begin reading from the file and sending messages to the queue every second    