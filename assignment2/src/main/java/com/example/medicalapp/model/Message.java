package com.example.medicalapp.model;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@JsonIdentityInfo(generator = ObjectIdGenerators.IntSequenceGenerator.class, property = "@id", scope = Message.class)
public class Message {
	
	private String patientId;
	private String startTime;
	private String endTime;
	private String activity;
	
	public Message(String patientId, String startTime, String endTime, String activity) {
		super();
		this.patientId = patientId;
		this.startTime = startTime;
		this.endTime = endTime;
		this.activity = activity;
	}
	
	public Message() {
		
	}
	
	public String getPatientId() {
		return patientId;
	}
	public void setPatientId(String patientId) {
		this.patientId = patientId;
	}
	public String getStartTime() {
		return startTime;
	}
	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}
	public String getEndTime() {
		return endTime;
	}
	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}
	public String getActivity() {
		return activity;
	}
	public void setActivity(String activity) {
		this.activity = activity;
	}

	@Override
	public String toString() {
		return "Message [patientId=" + patientId + ", startTime=" + startTime + ", endTime=" + endTime + 
				", activity=" + activity +"]";
	}
}
