package com.example.medicalapp.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.medicalapp.model.Medication;

@Repository
public interface MedicationsRepository  extends JpaRepository<Medication, String>{

}
