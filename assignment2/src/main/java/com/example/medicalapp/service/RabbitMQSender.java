package com.example.medicalapp.service;

import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.example.medicalapp.model.Message;

@Service
public class RabbitMQSender {
	
	@Autowired
	private AmqpTemplate rabbitTemplate;
	
	@Value("${rabbitmq.exchange}")
	private String exchange;
	
	@Value("${rabbitmq.routingKey}")
	private String routingkey;	
	
	public void send(Message msg) {
		rabbitTemplate.convertAndSend(exchange, routingkey, msg);
		System.out.println("Send msg = " + msg);
	    
	}
}
