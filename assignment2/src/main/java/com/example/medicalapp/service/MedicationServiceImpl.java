package com.example.medicalapp.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.medicalapp.model.Medication;
import com.example.medicalapp.repository.MedicationsRepository;

@Service(value = "medicationService")
public class MedicationServiceImpl implements MedicationService{
	@Autowired
	private MedicationsRepository medicationRepository;

	public List<Medication> findAll() {
		List<Medication> list = new ArrayList<>();
		medicationRepository.findAll().iterator().forEachRemaining(list::add);
		return list;
	}

	@Override
	public void delete(Medication p) {
		medicationRepository.delete(p);
	}


	@Override
	public Medication getMedicationById(String id) {
		Optional<Medication> optionalMedication = medicationRepository.findById(id);
		return optionalMedication.isPresent() ? optionalMedication.get() : null;
	}

    @Override
    public Medication update(Medication medicationDetails) throws Exception {
	    Medication updatedMedication = medicationRepository.save(medicationDetails);
	    return updatedMedication;
	    
    }

    @Override
    public Medication save(Medication medication) {
        return medicationRepository.save(medication);
    }
}
